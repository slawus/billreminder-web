<?php


namespace BillReminder\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Response;

class ExceptionController extends JSONController
{
    /**
     * Converts an Exception to a JSON Response.
     *
     * @param FlattenException $exception A FlattenException instance
     *
     * @return Response
     * @Rest\View
     */
    public function showAction(FlattenException $exception)
    {
        $body = $this->errorBody($exception->getStatusCode());
        if ($exception->getStatusCode() === 403) {
            $body = $this->errorBody(403, $exception->getMessage(), null, 'access_denied');
        };

        return new Response(\json_encode($body), $exception->getStatusCode(), $this->getJSONHeaders());
    }

    /**
     * Generate JSON Response headers.
     *
     * @return array
     */
    private function getJSONHeaders()
    {
        return array(
            'Content-Type'  => 'application/json;charset=UTF-8',
            'Cache-Control' => 'no-store',
            'Pragma'        => 'no-cache',
        );
    }
} 