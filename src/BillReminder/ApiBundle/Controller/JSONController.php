<?php


namespace BillReminder\ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;

class JSONController extends FOSRestController
{

    /**
     * @Rest\View
     */
    protected function errorBody($httpErrorCode = Response::HTTP_BAD_REQUEST, $errorDescription = null, $extras = null, $errorToken = null)
    {
        if ($errorDescription === null) {
            $errorDescription = Response::$statusTexts[$httpErrorCode];
        }
        $errorDescription = \trim($errorDescription, '.,!?;-');

        if ($errorToken === null) {
            $errorType = (int)($httpErrorCode / 100);
            switch ($errorType) {
                case 1:
                    $errorToken = 'informational_error';
                    break;
                case 2:
                    $errorToken = 'successful_error';
                    break;
                case 3:
                    $errorToken = 'redirection_error';
                    break;
                case 4:
                    $errorToken = 'client_error';
                    break;
                case 5:
                    $errorToken = 'server_error';
                    break;
                default:
                    $httpErrorCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                    $errorToken = 'server_error';
                    $errorDescription = 'Wrong error code';
            }
        }

        $errorBody = array('error' => $errorToken, 'error_description' => $errorDescription, 'code' => $httpErrorCode);
        if ($extras !== null) {
            $errorBody['extras'] = $extras;
        }

        return $errorBody;
    }

    public function errorView($errorDescription = null, $httpErrorCode = Response::HTTP_BAD_REQUEST, $extras = null, $errorToken = null)
    {
        return $this->view($this->errorBody($httpErrorCode, $errorDescription, $extras, $errorToken), $httpErrorCode);
    }
} 