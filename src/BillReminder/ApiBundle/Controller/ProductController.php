<?php


namespace BillReminder\ApiBundle\Controller;

use BillReminder\ApiBundle\FormType\PhotoAddType;
use BillReminder\ApiBundle\FormType\ProductAddType;
use BillReminder\ApiBundle\FormType\ProductUpdateType;
use BillReminder\ApiBundle\FormType\SendLocalBillType;
use BillReminder\ApiBundle\Model\Request as Model;
use BillReminder\BaseBundle\Entity\Photo;
use BillReminder\BaseBundle\Entity\Product;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use League\Flysystem\Filesystem;
use League\Flysystem\Util;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class ProductController extends JSONController
{

    /**
     * Add new product.
     *
     * @ApiDoc(
     *      section="Product",
     *      description="Add new product",
     *      filters={
     *          {"name"="bill", "dataType"="file", "required"=true},
     *          {"name"="photos", "dataType"="array", "required"=false},
     *          {"name"="category", "dataType"="string", "required"=true},
     *          {"name"="warranty_length", "dataType"="integer", "required"=true},
     *          {"name"="warranty_end", "dataType"="date", "require"=true}
     *      }
     * )
     *
     * @Method("POST")
     * @Route("/product")
     * @Rest\View
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function addAction(Request $request)
    {
        //validate request
        $model = new Model\ProductAdd();
        $form = $this->createForm(new ProductAddType(), $model);
        $form->submit(\array_merge($request->request->all(), $request->files->all()));
        if (!$form->isValid()) {
            return $this->errorView('validation.error', 400, $form);
        }

        $product = new Product();
        $product->setCategory($model->getCategory());
        $product->setBill(new Photo($model->getBill()));
        $product->setOwner($this->getUser());
        $product->setWarrantyEnd($model->getWarrantyEnd());
        $product->setWarrantyLength($model->getWarrantyLength());
        $product->setCreatedAt($model->getCreatedAt());

        if (\is_array($model->getPhotos())) {
            foreach ($model->getPhotos() as $file) {
                $product->addPhoto(new Photo($file));
            }
        }

        $this->getDoctrine()->getManager()->persist($product);
        $this->getDoctrine()->getManager()->flush();

        return $this->view($product, 200);
    }

    /**
     * Add new product.
     *
     * @ApiDoc(
     *      section="Product",
     *      description="Update product",
     *      filters={
     *          {"name"="bill", "dataType"="file"},
     *          {"name"="category", "dataType"="string"},
     *          {"name"="warranty_length", "dataType"="integer"},
     *          {"name"="warranty_end", "dataType"="date"}
     *      }
     * )
     *
     * @Method("PUT")
     * @Route("/product/{id}")
     * @Rest\View
     *
     * @param int $id
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function updateAction($id, Request $request)
    {
        //validate request
        $model = new Model\ProductUpdate();
        $form = $this->createForm(new ProductUpdateType(), $model);
        $form->submit(\array_merge($request->request->all(), $request->files->all()));
        if (!$form->isValid()) {
            return $this->errorView('validation.error', 400, $form);
        }

        /** @var Product $product */
        $product = $this->getDoctrine()->getManager()->getRepository('BillReminderBaseBundle:Product')->find($id);
        if (!$product) {
            return $this->errorView('product.not_found', 404);
        }

        if ($product->getOwner() != $this->getUser()) {
            return $this->errorView(null, 403);
        }

        if($model->getBill()) {
            $product->setBill(new Photo($model->getBill()));
        }
        $fields = ['WarrantyEnd', 'WarrantyLength', 'Category'];
        foreach($fields as $field) {
            $getter = 'get'.$field;
            $setter = 'set'.$field;
            if($model->$getter()) {
                $product->$setter($model->$getter());
            }
        }
        $this->getDoctrine()->getManager()->flush();

        return $this->view($product, 200);
    }

    /**
     * Add new photo.
     *
     * @ApiDoc(
     *      section="Product",
     *      description="Add new photo",
     *      filters={
     *          {"name"="photo", "dataType"="file", "required"=true},
     *      }
     * )
     *
     * @Method("POST")
     * @Route("/product/{id}/photo")
     * @Rest\View
     *
     * @param int     $id
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function addPhotoAction($id, Request $request)
    {
        //validate request
        $model = new Model\PhotoAdd();
        $form = $this->createForm(new PhotoAddType(), $model);
        $form->submit(\array_merge($request->request->all(), $request->files->all()));
        if (!$form->isValid()) {
            return $this->errorView('validation.error', 400, $form);
        }

        /** @var Product $product */
        $product = $this->getDoctrine()->getManager()->getRepository('BillReminderBaseBundle:Product')->find($id);
        if (!$product) {
            return $this->errorView('product.not_found', 404);
        }

        if ($product->getOwner() != $this->getUser()) {
            return $this->errorView(null, 403);
        }

        $photo = new Photo($model->getPhoto());
        $product->addPhoto($photo);

        if ($product->getPhotos()->count() > $this->get('service_container')->getParameter('billreminder.photos_limit')
        ) {
            return $this->view('product.photos_limit_exceeded', 400);
        }

        $this->getDoctrine()->getManager()->flush();

        return $this->view($photo, 200);
    }

    /**
     * Remove photo.
     *
     * @ApiDoc(
     *      section="Product",
     *      description="Remove photo",
     * )
     *
     * @Method("DELETE")
     * @Route("/product/{id}/photo/{photoId}")
     * @Rest\View
     *
     * @param int $id
     * @param int $photoId
     *
     * @return \FOS\RestBundle\View\View
     */
    public function removePhotoAction($id, $photoId)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Product $product */
        $product = $em->getRepository('BillReminderBaseBundle:Product')->find($id);
        if (!$product) {
            return $this->errorView('product.not_found', 404);
        }

        if ($product->getOwner() != $this->getUser()) {
            return $this->errorView(null, 403);
        }

        $photo = $em->getRepository('BillReminderBaseBundle:Photo')->find($photoId);
        if (!$photo) {
            return $this->errorView('photo.not_found', 404);
        }

        $em->remove($photo);
        $em->flush();

        return $this->view('photo.removed', 200);
    }

    /**
     * Remove photo.
     *
     * @ApiDoc(
     *      section="Product",
     *      description="Remove photo",
     * )
     *
     * @Method("DELETE")
     * @Route("/product/{id}")
     * @Rest\View
     *
     * @param int $id
     *
     * @return \FOS\RestBundle\View\View
     */
    public function removeProductAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Product $product */
        $product = $em->getRepository('BillReminderBaseBundle:Product')->find($id);
        if (!$product) {
            return $this->errorView('product.not_found', 404);
        }

        if ($product->getOwner() != $this->getUser()) {
            return $this->errorView(null, 403);
        }

        $em->remove($product);
        $em->flush();

        return $this->view('product.removed', 200);
    }

    /**
     * Return all products;
     *
     * @ApiDoc(
     *      section="Product",
     *      description="Get all products",
     *      statusCodes={
     *      }
     * )
     *
     * @Method("GET")
     * @Route("/products")
     * @Rest\View
     */
    public function getAllAction()
    {
        $products = $this->getDoctrine()->getManager()
                         ->getRepository('BillReminderBaseBundle:Product')
                         ->findBy(['owner' => $this->getUser()]);

        return $this->view($products, 200);
    }


    /**
     * Sends email to user with bill as pdf.
     *
     * @ApiDoc(
     *      section="Product",
     *      description="Sends email to user with bill as pdf",
     *      statusCodes={
     *      }
     * )
     * @Method("GET")
     * @Route("/product/{id}/send_bill", requirements={"id"="\d+"})
     * @Rest\View
     *
     * @param $id
     *
     * @return \FOS\RestBundle\View\View
     */
    public function sendBillAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var Product $product */
        $product = $em->getRepository('BillReminderBaseBundle:Product')->find($id);
        if (!$product) {
            return $this->errorView('product.not_found', 404);
        }

        if ($product->getOwner() != $this->getUser()) {
            return $this->errorView(null, 403);
        }

        $html = $this->get('templating')->render('BillReminderBaseBundle:Pdf:bill.html.twig', ['product' => $product]);
        $pdfData = $this->get('spraed.pdf.generator')->generatePDF($html);
        $attachment = \Swift_Attachment::newInstance($pdfData, 'bill.pdf', 'application/pdf');

        $emailContent = $this->get('templating')
                             ->render('BillReminderBaseBundle:Email:Bill.txt.twig');
        $from = $this->get('service_container')->getParameter('billreminder.mailer_email');
        $to = $product->getOwner()->getEmail();
        $this->get('billreminder.base.mailer_helper')
             ->sendEmailMessage($emailContent, $from, $to, null, [], [$attachment]);

        return $this->view('email.send', 200);
    }

    /**
     * Sends email to user with bill as pdf.
     *
     * @ApiDoc(
     *      section="Product",
     *      description="Sends email to user with bill as pdf",
     *      statusCodes={
     *      }
     * )
     * @Method("POST")
     * @Route("/product/local/send_bill")
     * @Rest\View
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function sendBillForLocalProductAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        //validate request
        $model = new Model\SendLocalBill();
        $form = $this->createForm(new SendLocalBillType(), $model);
        $form->submit(\array_merge($request->request->all(), $request->files->all()));
        if (!$form->isValid()) {
            return $this->errorView('validation.error', 400, $form);
        }

        /** @var Filesystem $filesystem */
        $filesystem = $this->get('oneup_flysystem.temp_filesystem');
        $fileName = md5(time().''.$model->getBill()->getBasename()).'.'.$model->getBill()->getClientOriginalName();
        $stream = fopen($model->getBill()->getRealPath(), 'r');
        $filesystem->writeStream($fileName, $stream);
        fclose($stream);

        $img = $filesystem->get($fileName);
        $html = $this->get('templating')->render('BillReminderBaseBundle:Pdf:local_bill.html.twig', ['billUri' => Util::normalizePath($img->getPath())]);
        $pdfData = $this->get('spraed.pdf.generator')->generatePDF($html);
        $filesystem->delete($img->getPath());

        $attachment = \Swift_Attachment::newInstance($pdfData, 'bill.pdf', 'application/pdf');
        $emailContent = $this->get('templating')
                             ->render('BillReminderBaseBundle:Email:Bill.txt.twig');
        $from = $this->get('service_container')->getParameter('billreminder.mailer_email');
        $to = $this->getUser()->getEmail();
        $this->get('billreminder.base.mailer_helper')
             ->sendEmailMessage($emailContent, $from, $to, null, [], [$attachment]);

        return $this->view('email.send', 200);
    }
} 