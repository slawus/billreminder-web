<?php

namespace BillReminder\ApiBundle\Controller;

use BillReminder\ApiBundle\FormType\ChangePasswordType;
use BillReminder\ApiBundle\FormType\ForgotPasswordType;
use BillReminder\ApiBundle\FormType\LoginType;
use BillReminder\ApiBundle\FormType\RegisterType;
use BillReminder\ApiBundle\Model\Request as Model;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\UserBundle\Model\User;
use FOS\UserBundle\Model\UserInterface;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;


class SecurityController extends JSONController
{

    /**
     * Register new user.
     *
     * @ApiDoc(
     *      section="Security",
     *      description="Register new user",
     *      filters={
     *          {"name"="email", "dataType"="string", "required"=true},
     *          {"name"="password", "dataType"="string", "required"=true},
     *          {"name"="client_id", "dataType"="string", "required"=true}
     *      },
     *      statusCodes={
     *          200="user_register.success",
     *          400="Validation error",
     *          403="error.wrong_client",
     *          409="user_register.error.email_used"
     *      }
     * )
     *
     * @Method("POST")
     * @Route("/register")
     * @Rest\View
     */
    public function registerAction(Request $request)
    {
        //validate request
        $model = new Model\Register();
        $form = $this->createForm(new RegisterType(), $model);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            return $this->errorView('error.validation', 400, $form);
        }

        //get client
        $clientManager = $this->get('fos_oauth_server.client_manager');
        $client = $clientManager->findClientByPublicId($model->getClientId());
        if (!$client) {
            return $this->errorView('error.wrong_client', 403);
        }

        //check if email already used
        $userManager = $this->get('fos_user.user_manager');
        $aeUser = $userManager->findUserByEmail($model->getEmail());
        if ($aeUser) {
            return $this->errorView('user_register.error.email_used', 409);
        }

        //register user
        $user = $userManager->createUser();
        $user->setEmail($model->getEmail());
        $user->setPlainPassword($model->getPassword());
        $user->setEnabled(true);
        $userManager->updateUser($user);

        //create token
        $oauthServer = $this->get('fos_oauth_server.server');
        $token = $oauthServer->createAccessToken($client, $user);

        return $this->view(['token' => $token, 'user_id' => $user->getId()], 200);
    }

    /**
     * @ApiDoc(
     *      section="Security",
     *      description="Login to system",
     *      filters={
     *          {"name"="email", "dataType"="string", "required"=true},
     *          {"name"="password", "dataType"="string", "required"=true},
     *          {"name"="client_id", "dataType"="string", "required"=true}
     *      },
     *      statusCodes={
     *          200="success",
     *          400="Validation error",
     *          403="login.error.wrong_credentials|error.wrong_client"
     *      }
     * )
     *
     * @Route("/login")
     * @Method("POST")
     */
    public function loginAction(Request $request)
    {
        //validate request
        $model = new Model\Login();
        $form = $this->createForm(new LoginType(), $model);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            return $this->errorView('error.validation', 400, $form);
        }

        //load user
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->findUserByUsernameOrEmail($model->getEmail());
        if (!$user) {
            return $this->errorView('login.error.wrong_credentials', 403);
        }

        //authenticate
        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        if (!$encoder->isPasswordValid($user->getPassword(), $model->getPassword(), $user->getSalt())) {
            return $this->errorView('login.error.wrong_credentials', 403);
        }

        //get client
        $clientManager = $this->get('fos_oauth_server.client_manager');
        $client = $clientManager->findClientByPublicId($model->getClientId());
        if (!$client) {
            return $this->errorView('error.wrong_client', 403);
        }

        //create token
        $oauthServer = $this->get('fos_oauth_server.server');
        $token = $oauthServer->createAccessToken($client, $user);

        return $this->view(['token' => $token, 'user_id' => $user->getId()], 200);
    }

    /**
     * Change user password.
     *
     * @ApiDoc(
     *      section="Security",
     *      description="Change password",
     *      filters={
     *          {"name"="old_password", "dataType"="string", "required"=true},
     *          {"name"="new_password", "dataType"="string", "required"=true},
     *      },
     *      statusCodes={
     *          200="success",
     *          400="Validation error",
     *          403="login.error.wrong_credentials"
     *      }
     * )
     *
     * @Route("/change_password")
     * @Method("POST")
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function changePasswordAction(Request $request)
    {
        //validate request
        $model = new Model\ChangePassword();
        $form = $this->createForm(new ChangePasswordType(), $model);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            return $this->errorView('error.validation', 400, $form);
        }

        //authenticate
        /** @var User $user */
        $user = $this->getUser();
        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        if (!$encoder->isPasswordValid($user->getPassword(), $model->getOldPassword(), $user->getSalt())) {
            return $this->errorView('login.error.wrong_credentials', 403);
        }

        $user->setPlainPassword($model->getNewPassword());
        $this->get('fos_user.user_manager')->updateUser($user, true);

        return $this->view('password.changed', 200);
    }

    /**
     * Request password reset.
     *
     * @ApiDoc(
     *      section="Security",
     *      description="Request password reset",
     *      filters={
     *          {"name"="email", "dataType"="string", "required"=true},
     *          {"name"="client_id", "dataType"="string", "required"=true},
     *      },
     *      statusCodes={
     *          200="success",
     *          400="Validation error",
     *          403="login.error.wrong_credentials"
     *      }
     * )
     *
     * @Route("/reset_password")
     * @Method("POST")
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function forgotPasswordAction(Request $request)
    {
        //validate request
        $model = new Model\ForgotPassword();
        $form = $this->createForm(new ForgotPasswordType(), $model);
        $form->submit($request->request->all());
        if (!$form->isValid()) {
            return $this->errorView('error.validation', 400, $form);
        }

        /** @var $user UserInterface */
        $user = $this->get('fos_user.user_manager')->findUserByUsernameOrEmail($model->getEmail());
        if (null === $user) {
            return $this->errorView('user.not_found', 404);
        }

        if ($user->isPasswordRequestNonExpired($this->container->getParameter('fos_user.resetting.token_ttl'))) {
            return $this->errorView('password_reset.already_requested');
        }

        if (null === $user->getConfirmationToken()) {
            /** @var $tokenGenerator \FOS\UserBundle\Util\TokenGeneratorInterface */
            $tokenGenerator = $this->get('fos_user.util.token_generator');
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }

        $this->get('fos_user.mailer')->sendResettingEmailMessage($user);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('fos_user.user_manager')->updateUser($user);

        return $this->view('password.reset_requested');
    }
} 