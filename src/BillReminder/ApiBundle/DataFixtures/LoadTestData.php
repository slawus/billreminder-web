<?php

namespace BillReminder\ApiBundle\DataFixtures;

use BillReminder\ApiBundle\Entity\AccessToken;
use BillReminder\ApiBundle\Entity\Client;
use BillReminder\BaseBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class LoadTestData implements FixtureInterface
{
    const CLIENT_ID = '1_1234567890';
    const USER_EMAIL = 'test@test.dev';
    const USER_PASSWORD = 'asdas&^*^%Ad12';
    const ACCESS_TOKEN = 'YzZiYWE4OGQyNWQ4Y2JmZjcxYjE4MDE0NDYzOGFmNzIzODdjMjRhMDdkYjEwY2U4NDc3MDU5ZjdhYTdhZDMyNA';

    public function load(ObjectManager $manager)
    {
        $client = new Client();
        $client->setName('test');
        $client->setRandomId(explode('_',self::CLIENT_ID)[1]);
        $manager->persist($client);

        $user = new User();
        $user->setEmail(self::USER_EMAIL);
        $user->setPlainPassword(self::USER_PASSWORD);
        $user->setEnabled(true);
        $manager->persist($user);

        $accessToken = new AccessToken();
        $accessToken->setClient($client);
        $accessToken->setUser($user);
        $accessToken->setToken(self::ACCESS_TOKEN);
        $accessToken->setScope(null);
        $date = new \DateTime();
        $date->modify('+1 month');
        $accessToken->setExpiresAt($date->getTimestamp());
        $manager->persist($accessToken);

        $manager->flush();
    }
} 