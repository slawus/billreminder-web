<?php

namespace BillReminder\ApiBundle\EventListeners;

use BillReminder\BaseBundle\Entity\Photo;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\PreSerializeEvent;
use Symfony\Component\HttpFoundation\RequestStack;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;

class SerializationSubstriber implements EventSubscriberInterface
{

    /**
     * @var UploaderHelper
     */
    private $vichUploaderHelper;
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * Constructor.
     *
     * @param UploaderHelper $vichUploaderHelper
     * @param RequestStack   $requestStack
     */
    public function __construct(UploaderHelper $vichUploaderHelper, RequestStack $requestStack)
    {
        $this->vichUploaderHelper = $vichUploaderHelper;
        $this->requestStack = $requestStack;
    }

    /**
     * Returns the events to which this class has subscribed.
     *
     * Return format:
     *     array(
     *         array('event' => 'the-event-name', 'method' => 'onEventName', 'class' => 'some-class', 'format' => 'json'),
     *         array(...),
     *     )
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [['event' => 'serializer.pre_serialize', 'class' => 'BillReminder\BaseBundle\Entity\Photo', 'method' => 'onPreSerialize']];
    }

    public function onPreSerialize(PreSerializeEvent $event)
    {
        /** @var Photo $photo */
        $photo = $event->getObject();
        $photo->setUrl($this->requestStack->getCurrentRequest()->getUriForPath($this->vichUploaderHelper->asset($photo, 'product_photo')));
    }
}