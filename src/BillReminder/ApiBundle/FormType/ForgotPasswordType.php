<?php


namespace BillReminder\ApiBundle\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ForgotPasswordType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'email')
                ->add('client_id', 'text');

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'forgot_password';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'BillReminder\ApiBundle\Model\Request\ForgotPassword',
            'csrf_protection' => false
        ));
    }
}