<?php

namespace BillReminder\ApiBundle\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class LoginType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('email', 'email')
                ->add('password', 'password')
                ->add('client_id', 'text');

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'login';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'BillReminder\ApiBundle\Model\Request\Login',
            'csrf_protection' => false
        ));
    }
}