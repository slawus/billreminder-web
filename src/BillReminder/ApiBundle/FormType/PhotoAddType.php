<?php


namespace BillReminder\ApiBundle\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PhotoAddType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('photo', 'file');

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'product_add';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'BillReminder\ApiBundle\Model\Request\PhotoAdd',
            'csrf_protection' => false
        ));
    }
}