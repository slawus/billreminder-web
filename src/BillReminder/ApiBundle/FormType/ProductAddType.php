<?php

namespace BillReminder\ApiBundle\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductAddType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('bill', 'file')
                ->add('photos', 'file', [
                    'multiple' => 'multiple'
                ])
                ->add('category', 'text')
                ->add('created_at', 'date', ['widget' => 'single_text',
                                             'format' => 'yyyy-MM-dd'])
                ->add('warranty_length', 'integer')
                ->add('warranty_end', 'date', ['widget' => 'single_text',
                                               'format' => 'yyyy-MM-dd']);

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'product_add';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'BillReminder\ApiBundle\Model\Request\ProductAdd',
            'csrf_protection' => false
        ));
    }
}