<?php

namespace BillReminder\ApiBundle\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductUpdateType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('bill', 'file')
                ->add('category', 'text')
                ->add('warranty_length', 'integer')
                ->add('warranty_end', 'date', ['widget' => 'single_text',
                                               'format' => 'yyyy-MM-dd']);

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'product_update';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'BillReminder\ApiBundle\Model\Request\ProductUpdate',
            'csrf_protection' => false
        ));
    }
}