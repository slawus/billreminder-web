<?php


namespace BillReminder\ApiBundle\FormType;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SendLocalBillType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('bill', 'file');

        return $builder;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'send_local_bill';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'      => 'BillReminder\ApiBundle\Model\Request\SendLocalBill',
            'csrf_protection' => false
        ));
    }
}