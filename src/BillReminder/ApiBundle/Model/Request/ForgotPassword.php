<?php

namespace BillReminder\ApiBundle\Model\Request;

use Symfony\Component\Validator\Constraints as Assert;

class ForgotPassword {
    /**
     * @Assert\NotNull()
     * @Assert\Email()
     *
     * @var string
     */
    protected $email;

    /**
     * @Assert\NotNull(),
     * @Assert\Type(type="string")
     *
     * @var string
     */
    protected $clientId;

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }
}