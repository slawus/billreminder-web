<?php


namespace BillReminder\ApiBundle\Model\Request;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class PhotoAdd
{

    /**
     * @Assert\Image
     * @Assert\NotBlank
     *
     * @var File
     */
    protected $photo;

    /**
     * @return File
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param File $photo
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

}