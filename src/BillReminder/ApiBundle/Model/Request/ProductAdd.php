<?php

namespace BillReminder\ApiBundle\Model\Request;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class ProductAdd
{

    /**
     * @Assert\Choice(choices={"electronics", "clothes", "shoes"})
     * @Assert\NotBlank
     *
     * @var string
     */
    protected $category;

    /**
     * @Assert\Image
     * @Assert\NotBlank
     *
     * @var File
     */
    protected $bill;

    /**
     * @Assert\All({
     *      @Assert\Image
     * })
     *
     * @var array
     */
    protected $photos;

    /**
     * @Assert\Type(type="integer")
     * @Assert\NotBlank
     */
    protected $warrantyLength;

    /**
     * @Assert\Type(type="Datetime")
     * @Assert\NotBlank
     */
    protected $warrantyEnd;

    /**
     * @Assert\Type(type="Datetime")
     * @Assert\NotBlank
     */
    protected $createdAt;

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return File
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * @param File $bill
     */
    public function setBill($bill)
    {
        $this->bill = $bill;
    }

    /**
     * @return array
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @param array $photos
     */
    public function setPhotos($photos)
    {
        $this->photos = $photos;
    }

    /**
     * @return int
     */
    public function getWarrantyLength()
    {
        return $this->warrantyLength;
    }

    /**
     * @param int $warrantyLength
     */
    public function setWarrantyLength($warrantyLength)
    {
        $this->warrantyLength = $warrantyLength;
    }

    /**
     * @return \Datetime
     */
    public function getWarrantyEnd()
    {
        return $this->warrantyEnd;
    }

    /**
     * @param \Datetime $warrantyEnd
     */
    public function setWarrantyEnd($warrantyEnd)
    {
        $this->warrantyEnd = $warrantyEnd;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
} 