<?php

namespace BillReminder\ApiBundle\Model\Request;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class ProductUpdate
{
    /**
     * @Assert\Choice(choices={"electronics", "clothes", "shoes"})
     *
     * @var string
     */
    protected $category;

    /**
     * @Assert\Image
     *
     * @var File
     */
    protected $bill;

    /**
     * @Assert\Type(type="integer")
     */
    protected $warrantyLength;

    /**
     * @Assert\Type(type="Datetime")
     */
    protected $warrantyEnd;

    /**
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param string $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return File
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * @param File $bill
     */
    public function setBill($bill)
    {
        $this->bill = $bill;
    }

    /**
     * @return mixed
     */
    public function getWarrantyLength()
    {
        return $this->warrantyLength;
    }

    /**
     * @param mixed $warrantyLength
     */
    public function setWarrantyLength($warrantyLength)
    {
        $this->warrantyLength = $warrantyLength;
    }

    /**
     * @return mixed
     */
    public function getWarrantyEnd()
    {
        return $this->warrantyEnd;
    }

    /**
     * @param mixed $warrantyEnd
     */
    public function setWarrantyEnd($warrantyEnd)
    {
        $this->warrantyEnd = $warrantyEnd;
    }
}