<?php


namespace BillReminder\ApiBundle\Model\Request;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;

class SendLocalBill
{

    /**
     * @Assert\Image
     * @Assert\NotBlank
     *
     * @var File
     */
    protected $bill;

    /**
     * @return File
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * @param File $bill
     */
    public function setBill($bill)
    {
        $this->bill = $bill;
    }

}