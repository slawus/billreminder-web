<?php


namespace BillReminder\ApiBundle\Tests\Controller;

use BillReminder\ApiBundle\DataFixtures\LoadTestData as TestData;
use BillReminder\ApiBundle\Tests\WebTestCase;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ProductControllerTest extends WebTestCase
{

    static $product;
    static $photo;

    public function loadData()
    {
        static::createClient();

        // add all your doctrine fixtures classes
        $classes = array(
            'BillReminder\ApiBundle\DataFixtures\LoadTestData'
        );

        $this->loadFixtures($classes);
    }

    public function testProductAdd()
    {
        $this->loadData();

        $path = 'api/product';

        $client = $this->makeClient();
        $client->request('POST', $path, [
            'category'        => 'clothes',
            'warranty_end'    => '2015-01-01',
            'warranty_length' => 365 * 24 * 60 * 60,
            'created_at' => '2014-02-01'
        ], [
            'bill' => new UploadedFile($this->getContainer()->get('kernel')
                                            ->getRootDir() . '/../src/BillReminder/ApiBundle/Tests/Assets/bill.jpg', 'bill.jpg')
        ], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
        ]);

        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        self::$product = json_decode($response->getContent(), true);
    }

    /**
     * @depends testProductAdd
     */
    public function testGetAllProducts()
    {
        $path = 'api/products';

        $client = $this->makeClient();
        $client->request('GET', $path, [], [], ['HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN]);
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        $data = \json_decode($response->getContent(), true);
        $this->assertCount(1, $data);
    }

    /**
     * @depends testProductAdd
     */
    public function testUpdateProduct()
    {
        $path = 'api/product/' . self::$product['id'];

        $client = $this->makeClient();
        $client->request('PUT', $path, [
            'category' => 'shoes'
        ], [], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
        ]);

        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        $product = $this->getContainer()->get('doctrine.orm.entity_manager')
                        ->getRepository('BillReminderBaseBundle:Product')->find(self::$product['id']);
        $this->assertEquals(self::$product['warranty_length'], $product->getWarrantyLength());
        $this->assertEquals('shoes', $product->getCategory());
    }

    /**
     * @depends testProductAdd
     */
    public function testBillEmailSend()
    {
        $path = 'api/product/' . self::$product['id'] . '/send_bill';
        $client = $this->makeClient();
        $client->enableProfiler();
        $client->request('GET', $path, [], [], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
        ]);
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertEquals(1, $mailCollector->getMessageCount());

        $message = $mailCollector->getMessages()[0];
    }

    public function testLocalBillEmailSend()
    {
        $path = 'api/product/local/send_bill';
        $client = $this->makeClient();
        $client->enableProfiler();
        $client->request('POST', $path, [], [
            'bill' => new UploadedFile($this->getContainer()->get('kernel')
                                            ->getRootDir() . '/../src/BillReminder/ApiBundle/Tests/Assets/bill.jpg', 'bill.jpg')
        ], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
        ]);
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertEquals(1, $mailCollector->getMessageCount());

        $message = $mailCollector->getMessages()[0];
    }

    /**
     * @depends testProductAdd
     */
    public function testPhotoAdd()
    {
        $path = 'api/product/' . self::$product['id'] . '/photo';

        $client = $this->makeClient();
        $client->request('POST', $path, [], [
            'photo' => new UploadedFile($this->getContainer()->get('kernel')
                                             ->getRootDir() . '/../src/BillReminder/ApiBundle/Tests/Assets/bill.jpg', 'bill.jpg')
        ], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
        ]);

        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        self::$photo = json_decode($response->getContent(), true);
    }

    /**
     * @depends testPhotoAdd
     */
    public function testPhotoDelete()
    {
        $path = 'api/product/' . self::$product['id'] . '/photo/' . self::$photo['id'];

        $client = $this->makeClient();
        $client->request('DELETE', $path, [], [], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
        ]);

        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
    }

    /**
     * @depends testPhotoDelete
     */
    public function testProductDelete()
    {
        //add photo
        $path = 'api/product/' . self::$product['id'] . '/photo';
        $client = $this->makeClient();
        $client->request('POST', $path, [], [
            'photo' => new UploadedFile($this->getContainer()->get('kernel')
                                             ->getRootDir() . '/../src/BillReminder/ApiBundle/Tests/Assets/bill.jpg', 'bill.jpg')
        ], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
        ]);

        //remove product
        $path = 'api/product/' . self::$product['id'];
        $client = $this->makeClient();
        $client->request('DELETE', $path, [], [], [
            'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
        ]);

        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
    }
}
