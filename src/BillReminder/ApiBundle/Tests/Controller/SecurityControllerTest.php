<?php

namespace BillReminder\ApiBundle\Tests\Controller;

use BillReminder\ApiBundle\DataFixtures\LoadTestData as TestData;
use BillReminder\ApiBundle\Tests\WebTestCase;

class SecurityControllerTest extends WebTestCase
{

    const REGISTER_URI = 'api/register';
    const LOGIN_URI = 'api/login';
    const CHANGE_PASSWORD_URI = 'api/change_password';
    const FORGOT_PASSWORD_URI = 'api/reset_password';

    public function loadData()
    {
        static::createClient();

        // add all your doctrine fixtures classes
        $classes = array(
            'BillReminder\ApiBundle\DataFixtures\LoadTestData'
        );

        $this->loadFixtures($classes);
    }

    public function setUp()
    {
        $this->loadData();
    }

    public function testUserRegister()
    {
        $email = 'test@test.org';
        $client = $this->makeClient();
        $client->request('POST', self::REGISTER_URI, [
            'email'     => $email,
            'password'  => 'password12',
            'client_id' => TestData::CLIENT_ID
        ]);
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
        $this->assertArrayHasKeys(['token', 'user_id'], json_decode($response->getContent(), true));

        $userManager = $this->getContainer()->get('fos_user.user_manager');
        $user = $userManager->findUserByEmail($email);
        $this->assertNotNull($user);
    }

    public function testUserRegisterWithWrongClient()
    {
        $client = $this->makeClient();
        $client->request('POST', self::REGISTER_URI, [
            'email'     => 'lol@test.org',
            'password'  => 'password12',
            'client_id' => 'Not_correct_client_id'
        ]);
        $response = $client->getResponse();

        $this->assertEquals(403, $response->getStatusCode());
    }

    public function testUserRegisterWithMissingData()
    {
        $client = $this->makeClient();
        $client->request('POST', self::REGISTER_URI, [
            'client_id' => TestData::CLIENT_ID
        ]);
        $response = $client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testUserLogin()
    {
        $client = $this->makeClient();
        $client->request('POST', self::LOGIN_URI, [
            'email'     => TestData::USER_EMAIL,
            'password'  => TestData::USER_PASSWORD,
            'client_id' => TestData::CLIENT_ID
        ]);
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        $responseData = \json_decode($response->getContent(), true);
        $this->assertArrayHasKeys(['token', 'user_id'], $responseData);
    }

    public function testPasswordChange()
    {
        $newPassword = "this is new p4ssword";
        $client = $this->makeClient();
        $client->request('POST', self::CHANGE_PASSWORD_URI, [
            'new_password'     => $newPassword,
            'old_password'  => TestData::USER_PASSWORD,
        ], [], [
                'HTTP_AUTHORIZATION' => 'Bearer ' . TestData::ACCESS_TOKEN
            ]
        );
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        //try to login with new password
        $client = $this->makeClient();
        $client->request('POST', self::LOGIN_URI, [
            'email'     => TestData::USER_EMAIL,
            'password'  => $newPassword,
            'client_id' => TestData::CLIENT_ID
        ]);
        $response = $client->getResponse();
        $this->assertTrue($response->isOk());
    }

    public function testUserLoginWithWrongPassword()
    {
        $wrongPassword = '123456';
        $client = $this->makeClient();
        $client->request('POST', self::LOGIN_URI, [
            'email'     => TestData::USER_EMAIL,
            'password'  => $wrongPassword,
            'client_id' => TestData::CLIENT_ID
        ]);

        $response = $client->getResponse();
        $this->assertEquals(403, $response->getStatusCode());
    }

    public function testPasswordResetRequest()
    {
        $client = $this->makeClient();
        $client->enableProfiler();
        $client->request('POST', self::FORGOT_PASSWORD_URI, [
            'email' => TestData::USER_EMAIL,
            'client_id' => TestData::CLIENT_ID
        ]);

        $response = $client->getResponse();
        $this->assertTrue($response->isOk());

        $mailCollector = $client->getProfile()->getCollector('swiftmailer');
        $this->assertEquals(1, $mailCollector->getMessageCount());

        $message = $mailCollector->getMessages()[0];
    }
} 