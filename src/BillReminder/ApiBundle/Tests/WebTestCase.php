<?php

namespace BillReminder\ApiBundle\Tests;

class WebTestCase extends \Liip\FunctionalTestBundle\Test\WebTestCase
{

    public function assertArrayHasKeys(array $keys, array $array)
    {
        foreach ($keys as $key) {
            $this->assertArrayHasKey($key, $array);
        }
    }
} 