<?php


namespace BillReminder\BaseBundle\Command;

use BillReminder\BaseBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmailAboutEndingWarrantiesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('br:email:ending_warranties')
            ->setDescription('Sends emails to users about ending warranties');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $before = new \DateTime();
        $before->modify('+7 days');
        $before->setTime(0, 0, 0);

        $after = $before;
        $after->modify('-1 days');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $qb = $em->getRepository('BillReminderBaseBundle:Product')->createQueryBuilder('p');
        $qb->select('p')
            ->where('p.warrantyEnd < :before')
            ->setParameter('before', $before)
            ->andWhere('p.warrantyEnd > :after')
            ->setParameter('after', $after);
        $productsWithEndingWarranties = $qb->getQuery()->getResult();

        $mailerHelper = $this->getContainer()->get('billreminder.base.mailer_helper');
        $template = 'BillReminderBaseBundle:Email:WarrantyEndReminder.txt.twig';
        $from = $this->getContainer()->getParameter('billreminder.mailer_email');

        /** @var Product $product */
        foreach ($productsWithEndingWarranties as $product) {
            $to = $product->getOwner()->getEmail();
            $rendered = $this->getContainer()->get('templating')->render(
                $template, array(
                    'email' => $to
                )
            );

            $mailerHelper->sendEmailMessage($rendered, $from, $to);
        }

        $output->writeln(sprintf('Successfully send %d emails.', \count($productsWithEndingWarranties)));
    }
}