<?php


namespace BillReminder\BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class SecurityController extends Controller
{
    /**
     * @Route("resetting/confirmation")
     */
    public function passwordRessetConfirmationAction()
    {
        return $this->render('BillReminderBaseBundle:Security:passwordresetConfirmation.html.twig');
    }
}