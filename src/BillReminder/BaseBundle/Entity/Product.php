<?php


namespace BillReminder\BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("none")
 */
class Product
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="BillReminder\BaseBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", nullable=false)
     * @Serializer\Exclude
     */
    protected $owner;

    /**
     * @ORM\OneToOne(targetEntity="BillReminder\BaseBundle\Entity\Photo", cascade={"all"})
     * @ORM\JoinColumn(name="bill_id", referencedColumnName="id")
     */
    protected $bill;

    /**
     * @ORM\ManyToMany(targetEntity="BillReminder\BaseBundle\Entity\Photo", cascade={"all"})
     * @ORM\JoinTable(name="product_photos",
     *      joinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="photo_id", referencedColumnName="id", unique=true, onDelete="CASCADE")}
     *      )
     */
    protected $photos;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $category;

    /**
     * @ORM\Column(type="date", name="warranty_end")
     */
    protected $warrantyEnd;

    /**
     * @ORM\Column(type="integer", name="warranty_length")
     */
    protected $warrantyLength;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     */
    protected $createdAt;

    /**
     * @ORM\Column(type="datetime", name="updated_at", nullable=true)
     */
    protected $updatedAt;

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photos = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return Product
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return string 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     * @return Product
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     * @return Product
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set owner
     *
     * @param \BillReminder\BaseBundle\Entity\User $owner
     * @return Product
     */
    public function setOwner(\BillReminder\BaseBundle\Entity\User $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \BillReminder\BaseBundle\Entity\User 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set bill
     *
     * @param \BillReminder\BaseBundle\Entity\Photo $bill
     * @return Product
     */
    public function setBill(\BillReminder\BaseBundle\Entity\Photo $bill = null)
    {
        $this->bill = $bill;

        return $this;
    }

    /**
     * Get bill
     *
     * @return \BillReminder\BaseBundle\Entity\Photo 
     */
    public function getBill()
    {
        return $this->bill;
    }

    /**
     * Add photos
     *
     * @param \BillReminder\BaseBundle\Entity\Photo $photos
     * @return Product
     */
    public function addPhoto(\BillReminder\BaseBundle\Entity\Photo $photos)
    {
        $this->photos[] = $photos;

        return $this;
    }

    /**
     * Remove photos
     *
     * @param \BillReminder\BaseBundle\Entity\Photo $photos
     */
    public function removePhoto(\BillReminder\BaseBundle\Entity\Photo $photos)
    {
        $this->photos->removeElement($photos);
    }

    /**
     * Get photos
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhotos()
    {
        return $this->photos;
    }

    /**
     * @return \Datetime
     */
    public function getWarrantyEnd()
    {
        return $this->warrantyEnd;
    }

    /**
     * @param \Datetime $warrantyEnd
     */
    public function setWarrantyEnd($warrantyEnd)
    {
        $this->warrantyEnd = $warrantyEnd;
    }

    /**
     * @return int
     */
    public function getWarrantyLength()
    {
        return $this->warrantyLength;
    }

    /**
     * @param int $warrantyLength
     */
    public function setWarrantyLength($warrantyLength)
    {
        $this->warrantyLength = $warrantyLength;
    }
}
