<?php
/**
 * Created by PhpStorm.
 * User: slawek
 * Date: 31.07.2014
 * Time: 13:32
 */

namespace BillReminder\BaseBundle\Service;

class MailerHelper
{
    protected $mailer;

    /**
     * Default controller.
     *
     * @param \Swift_Mailer $mailer
     */
    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param string $renderedTemplate
     * @param string $fromEmail
     * @param string $toEmail
     * @param null   $contentType
     * @param array  $bcc
     * @param array  $attachments
     */
    public function sendEmailMessage($renderedTemplate, $fromEmail, $toEmail, $contentType = null, $bcc = array(), $attachments = [])
    {
        // Render the email, use the first line as the subject, and the rest as the body
        $renderedLines = explode("\n", trim($renderedTemplate));
        $subject = $renderedLines[0];
        $body = implode("\n", array_slice($renderedLines, 1));

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($fromEmail)
            ->setTo($toEmail)
            ->setBcc($bcc)
            ->setBody($body, $contentType);

        foreach($attachments as $attachment) {
            $message->attach($attachment);
        }

        $this->mailer->send($message);
    }
}